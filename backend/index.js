const express = require('express')
const app = express ();
const bodyParser = require("body-parser")
const cors = require("cors")
var connection = require("./database");

app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({extended:true}));


app.post('/report/getReportData',(req,res)=>{
  console.log(req.body,'RRRRRR')
  let sqlGet =`SELECT id,sender,message,mobile_no,customer_name,send_time  FROM whatsapp_messages where id > 0 `
  if(req.body.mobileNo){
    sqlGet = sqlGet  + ` and mobile_no = '${req.body.mobileNo}'`
  }
  if(req.body.fromDate){
    sqlGet = sqlGet  + ` and Date(send_time) between '${req.body.fromDate}' and '${req.body.toDate}'`
  }

  if(req.body.vehicle_model){
    sqlGet = sqlGet +  `and message LIKE '%-> ${req.body.vehicle_model}%'`
  }
  
  sqlGet = sqlGet + ` order by send_time DESC`;
  console.log(sqlGet)
  connection.query(sqlGet,(error,result)=>{
    if(error) throw error
    res.send(result);
  
  });
   
});

app.post('/report/getmobilenumber',(req,res)=>{
  const sqlGet ="SELECT DISTINCT mobile_no FROM whatsapp_messages";
  connection.query(sqlGet,(error,result)=>{
    res.send(result);
  });
   
});

app.post('/report/vehicemodels',(req,res)=>{
  const sqlGet1 ="SELECT * FROM vehicle_models";
  connection.query(sqlGet1,(error,result)=>{
    res.send(result);
  });
});

app.listen(5000,()=>console.log("server is running on PORT 5000"))