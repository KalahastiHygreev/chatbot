import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  Form,
  Row,
  Col,
  Select,
  Table,
  DatePicker,
  Tooltip,
  Pagination,
} from "antd";
import { SearchOutlined, UndoOutlined } from "@ant-design/icons";
import "antd/dist/reset.css";
import "./App.css";
import axios from "axios";
import moment from "moment";
import { FileExcelFilled, DownloadOutlined } from "@ant-design/icons";
import { Excel } from "antd-table-saveas-excel";
import { Content } from "antd/es/layout/layout";



const App = () => {
  const [data, setData] = useState<any>([]);
  const [vehicle, setVehicle] = useState<any>([]);
  const [mobile, setMobile] = useState<any>([]);
  const [loading, setLoading] = useState(false);
  const [page,  setPage]  = useState(1)

  const [form] = Form.useForm();
  const { Option } = Select;
  const { RangePicker } = DatePicker;

  const onReset = () => {
    form.resetFields();
  };


  const columns = [
    { title: "S.NO",  ellipsis: true, width: "10px",render:(text:any,object:any,index:number)=>(page-1)*10+(index+1) },
    {
      title: "Date",
      dataIndex: "send_time",
      ellipsis: true,
      width: "15px",
      sorter: (a: any, b: any) =>
        moment(a.send_time).unix() - moment(b.send_time).unix(),
      render: (value: any) => moment(value).format("DD-MM-YY hh:mm a"),
    },
    {
      title: "Customer Name",
      dataIndex: "customer_name",
      ellipsis: true,
      width: "15px",
    },
    {
      title: "Mobile number",
      dataIndex: "mobile_no",
      ellipsis: true,
      width: "15px",
    },
    {
      title: "Request",
      dataIndex: "sender",
      ellipsis: true,
      width: "20px",
    },
  
    {
      title: "Response",
      dataIndex: "message",
      ellipsis: true,
      width: "40px",
    },
  ];

  const getData = () => {
    setLoading(true);
    const date = form.getFieldValue("fromDate");
    const fromDate = date ? new Date(date[0]) : null;
    const toDate = date ? new Date(date[1]) : null;
    const req = {
      mobileNo: form.getFieldValue("mobileNumber"),
      fromDate: fromDate ? moment(fromDate).format("YYYY-MM-DD") : null,
      toDate: toDate ? moment(toDate).format("YYYY-MM-DD") : null,
      vehicle_model: form.getFieldValue("vehiclemodel"),
    };
    axios
      .post("http://206.189.138.212:5000/report/getReportData", req)
      .then((response: any) => {
        console.log(response);
        setData(response.data);
        setLoading(false);
      });
  };

  useEffect(() => {
    getData();
  }, []);

  const vehiclemodel = () => {
    axios
      .post("http://206.189.138.212:5000/report/vehicemodels")
      .then((response: any) => {
        console.log(response.data);
        setVehicle(response.data);
      });
  };

  useEffect(() => {
    vehiclemodel();
  }, []);

  const getmobile = () => {
    axios
      .post("http://206.189.138.212:5000/report/getmobilenumber")
      .then((response: any) => {
        console.log(response.data);
        setMobile(response.data);
      });
  };

  useEffect(() => {
    getmobile();
  }, []);

  const handleExport = (e: any) => {
    e.preventDefault();

    // Formatting columns - to set default render
    const currentDate = new Date()
      .toISOString()
      .slice(0, 10)
      .split("-")
      .join("/");
    let cloneArr = columns.slice(0);
    cloneArr.splice(0, 1);
    cloneArr.splice(-1);

    let i = 1;
    const ExcelColumns = [
      {
        title: "S.NO",
        dataIndex: "id",
        render: (value: any) => {
          return i++;
        },
      },
      {
        title: "Date",
        dataIndex: "send_time",
        render: (value: any) => moment(value).format("DD-MM-YY hh:mm a"),
      },
      {
        title: "Customer Name",
        dataIndex: "customer_name",
      },
      {
        title: "Mobile number",
        dataIndex: "mobile_no",
      },
      {
        title: "Request",
        dataIndex: "sender",
      },

      {
        title: "Response",
        dataIndex: "message",
      },
    ];

    const excel = new Excel();
    excel.addSheet("Sheet1");
    excel.addRow();
    excel.addColumns(ExcelColumns);
    excel.addDataSource(data); // Add the gridData as the data source
    excel.saveAs(
      `Chatbot_Report-${moment(currentDate).format("DD-MM-YYYY")}.xlsx`
    );
  };

  return (
    <Content>
      <Card
        title={<span style={{ color: "white" }}>Report</span>}
        extra={
          <Button
            size="large"
            type="default"
            style={{ color: "green" }}
            onClick={handleExport}
            // icon={<FileExcelFilled />}
            icon={<DownloadOutlined />}
          >
            Export As Excel
          </Button>
        }
        style={{ textAlign: "center" }}
        headStyle={{ backgroundColor: "black", border: 0 }}
      >
        <Form form={form}>
          <Row gutter={[24, 24]}>
            <Col
              xs={{ span: 24 }}
              sm={{ span: 24 }}
              md={{ span: 5 }}
              lg={{ span: 5 }}
              xl={{ span: 5 }}
              style={{ margin: "1%" }}
            >
              <Form.Item
                name="fromDate"
                label="Date"
                initialValue={undefined}
                rules={[
                  {
                    required: false,
                    message: "Date",
                  },
                ]}
              >
                <RangePicker />
              </Form.Item>
            </Col>
            <Col
              xs={{ span: 24 }}
              sm={{ span: 24 }}
              md={{ span: 5 }}
              lg={{ span: 5 }}
              xl={{ span: 5 }}
              style={{ margin: "1%" }}
            >
              <Form.Item name="vehiclemodel" label="Vehicle model">
                <Select
                  showSearch
                  placeholder="Select Vehicle model"
                  optionFilterProp="children"
                  allowClear
                >
                  {vehicle.map((data1: any) => {
                    return (
                      <Option
                        key={data1.id}
                        value={data1.model_name}
                      >{`${data1.model_name}`}</Option>
                    );
                  })}
                </Select>
              </Form.Item>
            </Col>

            <Col
              xs={{ span: 24 }}
              sm={{ span: 24 }}
              md={{ span: 5 }}
              lg={{ span: 5 }}
              xl={{ span: 5 }}
              style={{ margin: "1%" }}
            >
              <Form.Item
                name="mobileNumber"
                label="Mobile number"
               
              >
                <Select
                  showSearch
                  placeholder="Select Mobilenumber"
                  optionFilterProp="children"
                  allowClear
                >
                  {mobile.map((data3: any, index: any) => {
                    return (
                      <Option
                        //key={Math.random()}
                        key={index}
                        value={data3.mobile_no}
                      >{`${data3.mobile_no}`}</Option>
                    );
                  })}
                </Select>
              </Form.Item>
            </Col>

            <Col
              xs={{ span: 24 }}
              sm={{ span: 24 }}
              md={{ span: 5 }}
              lg={{ span: 5 }}
              xl={{ span: 5 }}
              style={{ marginTop: 15 }}
            >
              <Button
                type="primary"
                icon={<SearchOutlined />}
                style={{ marginRight: 2, width: 100 }}
                htmlType="button"
                onClick={getData}
              >
                Search
              </Button>
              <Button
                type="primary"
                icon={<UndoOutlined />}
                htmlType="submit"
                onClick={onReset}
              >
                Reset
              </Button>
            </Col>
          </Row>
        </Form>
        <Table
          columns={columns}
          dataSource={data}
          loading={loading}
        pagination ={{pageSizeOptions: [10, 20, 50, 100, 200, 500, 1000], onChange(current){
            setPage(current);
            
        }}}
        />
      </Card>
    </Content>
  );
};

export default App;
